const gulp = require('gulp');
var browserSync = require('browser-sync').create();
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var pkg = require('./package.json');

/*
    -- TOP LEVEL FUNCITON --
    gulp.task - Define tasks
    gulp.src - Point to files to use
    gulp.dest - Points to folder to output
    gulp.watch - Watch files and folders for changes
*/

// Logs Message
gulp.task('message', function () {
    return console.log('Gulp is running');
});

// Copy All HTML files
gulp.task('copyHtml', function () {
    gulp.src('src/*.html')
        .pipe(gulp.dest('dist'));
})

// Optimize Images
gulp.task('imageMin', () =>
    gulp.src('src/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'))
);

// Minify JS
// gulp.task('minify', function() {
//     gulp.src('src/js/*.js')
//         .pipe(uglify())
//         .pipe(gulp.dest('dist/js'));
// });

//Compile Sass Not working
// gulp.task('sass'), function() {
//     gulp.src('src/sass/*.scss')
//         .pipe(sass().on('error', sass.logError))
//         // .pipe(sass())
//         .pipe(gulp.dest('dist/css'));
// }

// Compile SCSS
gulp.task('cleanCSS', function () {
    return gulp.src('src/sass/*.scss')
        .pipe(sass.sync({
            outputStyle: 'expanded'
        }).on('error', sass.logError))
        .pipe(gulp.dest('dist/css'))
});

//concat JS
gulp.task('scripts', function () {
    gulp.src('src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist/js'));
});

// gulp.task('css', function() {
//     gulp.src('src/css/*.css')
//         .pipe(concat('bundle.css'))
//         .pipe(uglify())
//         .pipe(gulp.dest('dist/css'));
// });

gulp.task('build', ['message', 'copyHtml', 'imageMin', 'scripts', 'cleanCSS']);


gulp.task('watch', function () {
    gulp.watch('src/js/*js', ['scripts']);
    gulp.watch('src/images/*', ['imageMin']);
    gulp.watch('src/sass/*.scss', ['cleanCSS']);
    gulp.watch('src/*.html', ['copyHtml']);
});